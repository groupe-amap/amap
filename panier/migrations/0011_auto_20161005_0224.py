# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0010_auto_20161005_0223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commande',
            name='coefficient_panier',
            field=models.DecimalField(blank=True, decimal_places=2, null=True, max_digits=3),
        ),
        migrations.AlterField(
            model_name='panier',
            name='coefficient_panier',
            field=models.DecimalField(blank=True, decimal_places=2, null=True, max_digits=3),
        ),
    ]
