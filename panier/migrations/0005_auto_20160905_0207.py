# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0004_remove_panier_quantite'),
    ]

    operations = [
        migrations.RenameField(
            model_name='commande',
            old_name='prix',
            new_name='prix_unitaire',
        ),
        migrations.AddField(
            model_name='commande',
            name='nom',
            field=models.CharField(default='Panier', max_length=255),
            preserve_default=False,
        ),
    ]
