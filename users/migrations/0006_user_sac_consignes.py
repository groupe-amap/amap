# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20160907_2256'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='sac_consignes',
            field=models.IntegerField(default=0),
        ),
    ]
